wordCount = 0
wordBank = {'Hello' : 0}
defaultCount = 0
f = open('document.txt')
for line in f:
    for word in line.split():
        tempCount = wordBank.get(word, defaultCount)
        wordBank[word] = tempCount + 1
f.close()

print "{} {}".format("Unique Words", len(wordBank.keys()))

